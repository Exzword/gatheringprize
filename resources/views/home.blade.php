@extends('layouts.app')

@section('title')
    Dashboard
@endsection

@section('section')
<section>
            <div class="content-wrapper">
                <div class="container-fluid">
                    <div class="page-header">
                        <h2>Dashboard</h2>
                    </div>
                    @if(\Session::has('success'))
                    <div class="col-md-12">
                        <div class="alert dark alert-icon bg-green alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <i class="material-icons">info_outline</i>
                            {{\Session::get('success')}}
                        </div>
                    </div>
                    @endif
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-b-30">
                            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                <!-- Indicators -->
                                <ol class="carousel-indicators">
                                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                </ol>

                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" role="listbox">
                                    <div class="item active">
                                        <img src="{{asset('images/image-gallery/1.jpg')}}" class="banner__dashboard" />
                                    </div>
                                    <div class="item">
                                        <img src="{{asset('images/image-gallery/2.jpg')}}" class="banner__dashboard" />
                                    </div>
                                    <div class="item">
                                        <img src="{{asset('images/image-gallery/3.jpg')}}" class="banner__dashboard" />
                                    </div>
                                </div>
                                            
                                <!-- Controls -->
                                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="page-header">
                        <h2>List Pemenang Hadiah</h2>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-12 m-b-10">
                            <a href="{{ route('home.export') }}" target="_blank" class="btn btn-default btn-outline waves-effect m-r-15">Export Excel</a>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                <div class="body table-responsive">
                                    <table class="table table-hover" style="margin-bottom: 0;">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Pemenang</th>
                                            <th>Hadiah</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($participants as $key => $participant)
                                        <tr>
                                            <th scope="row">{{$key + 1}}</th>
                                            <td>{{$participant->name}}</td>
                                            <td>{{$participant->prizes->name}}</td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div style="float: right;">
                                    {{ $participants->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection
